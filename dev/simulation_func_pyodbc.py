import sys
import configparser
import time
import sys
import csv
import os
from datetime import datetime
import pandas as pd
import numpy as np
import pyodbc as pdb
# from subprocess import call
# call(['bash', '/home/vineet/POCs/POC_6/Anaplan/anaplan-connect-1-3-3-3/SimulationCYIncrementalData-V1.sh'])
# call(['bash', '/home/vineet/POCs/POC_6/Anaplan/anaplan-connect-1-3-3-3/UpdatesReplica-V1.sh'])

COLUMN_MAPPINGS = {
	'Time' : 'Time',
	'Report Type' : 'Report Type',
	'Anaplan ID' : 'Anaplan ID',
	'To Export for BI - One Time Load' : 'To Export for BI - One Time Load',
	'Report Type' : 'Report Type',
	'Promo-In Changed Position ID' : 'Promo-In Changed Position ID',
	'Wrong Promo Identifier' : 'Wrong Promo Identifier',
	'Transferred Last Mapped ID of Rectified ID' : 'Transferred Last Mapped ID of Rectified ID',
	'Mapping History' : 'Mapping History',
	'Final Sub Band of Position' : 'Final Sub Band of Position',
	'Promo-Changed Sub Band' : 'Promo-Changed Sub Band',
	'Promo Out Date' : 'Promo Out Date',
	'Promo-In Effective Date' : 'Promo-In Effective Date',
	'Promo-In Position ID' : 'Promo-In Position ID',
	'Clear Promo' : 'Clear Promo',
	'Org Position Sub-Band' : 'Org Position Sub-Band',
	'AID' : 'AID',
	'Simulation Tag' : 'Simulation Tag',
	'Select for Simulation' : 'Select for Simulation',
	'Simulation Effective Date' : 'Simulation Effective Date',
	'Full Name' : 'Full Name',
	'Position ID' : 'Position ID',
	'Position ID (WFS)' : 'Position ID (WFS)',
	'Position Sub Band' : 'Position Sub Band',
	'Position Job Family' : 'Position Job Family',
	'Position Country' : 'Position Country',
	'Employee ID' : 'Employee ID',
	'Start Date' : 'Start Date',
	'LWD' : 'LWD',
	'T-Job Req ID Against PID' : 'T-Job Req ID Against PID',
	'JR ID' : 'JR ID',
	'JR Status' : 'JR Status',
	'Status' : 'Status',
	'Gender' : 'Gender',
	'Raised JR ID' : 'Raised JR ID',
	'Sub Vertical' : 'Sub Vertical',
	'BU SSU' : 'BU SSU',
	'LOB/Segment' : 'LOB/Segment',
	'Vertical' : 'Vertical',
	'Entity' : 'Entity',
	'Employee Sub Band' : 'Employee Sub Band',
	'Facility  Country' : 'Facility  Country',
	'Cost Center' : 'Cost Center',
	'Job Level' : 'Job Level',
	'Job Family' : 'Job Family',
	'Job Family Cluster' : 'Job Family Cluster',
	'Job Family Cluster (Current JF)' : 'Job Family Cluster (Current JF)',
	'City Cluster' : 'City Cluster',
	'International Region' : 'International Region',
	'Facility' : 'Facility',
	'Geozone' : 'Geozone',
	'Reporting Manager ID' : 'Reporting Manager ID',
	'Reporting Manager' : 'Reporting Manager',
	'Job Code Description' : 'Job Code Description',
	'Designation' : 'Designation',
	'MIS Cost Center' : 'MIS Cost Center',
	'Post Tag Cost Center' : 'Post Tag Cost Center',
	'Position End Date' : 'Position End Date',
	'Variable Pay Plan Type' : 'Variable Pay Plan Type',
	'Employee Class' : 'Employee Class',
	'Internal Specialization Name' : 'Internal Specialization Name',
	'Current Quartile Position' : 'Current Quartile Position',
	'Revised Quartile' : 'Revised Quartile',
	'Stack Rank Current' : 'Stack Rank Current',
	'Stack Rank - Planned' : 'Stack Rank - Planned',
	'Stack Rank Final' : 'Stack Rank Final',
	'Software Defined Networking (SDN)' : 'Software Defined Networking (SDN)',
	'Mobility' : 'Mobility',
	'Cloud Enabled Solution' : 'Cloud Enabled Solution',
	'Digitization (Robotics, Software, Analytics, Big Data)' : 'Digitization (Robotics, Software, Analytics, Big Data)',
	'Partnering (Service Co-creation, GTM, Supply Chain Mgmt.)' : 'Partnering (Service Co-creation, GTM, Supply Chain Mgmt.)',
	'Local Currency' : 'Local Currency',
	'FIXPAY-Amount' : 'FIXPAY-Amount',
	'RETIRALS-Amount' : 'RETIRALS-Amount',
	'Tata Group Joining Group' : 'Tata Group Joining Group',
	'Increment %' : 'Increment %',
	'Increment Override' : 'Increment Override',
	'Check For Zero Increment' : 'Check For Zero Increment',
	'OTE-(HeadCount/Offered)' : 'OTE-(HeadCount/Offered)',
	'OTE (Local Currency)' : 'OTE (Local Currency)',
	'OTE After Increment' : 'OTE After Increment',
	'Fringes (Local Currency)' : 'Fringes (Local Currency)',
	'Loaded Cost (Local Currency)' : 'Loaded Cost (Local Currency)',
	'FIXPAY-Amount  US $' : 'FIXPAY-Amount  US $',
	'RETIRALS-Amount  US $' : 'RETIRALS-Amount  US $',
	'VARPAY-Amount  US $' : 'VARPAY-Amount  US $',
	'OTE (USD)' : 'OTE (USD)',
	'Fringes (USD)' : 'Fringes (USD)',
	'Loaded Cost (USD)' : 'Loaded Cost (USD)',
	'Final Effective Date' : 'Final Effective Date',
	'Effective Start Date' : 'Effective Start Date',
	'Override Effective Start Date' : 'Override Effective Start Date',
	'New Manager ID' : 'New Manager ID',
	'Existing Manager' : 'Existing Manager',
	'Final Manager For Span of Control' : 'Final Manager For Span of Control',
	'Post Tag Country' : 'Post Tag Country',
	'Post Tag Job Family' : 'Post Tag Job Family',
	'Post Tag Sub Band' : 'Post Tag Sub Band',
	'Post Tag Currency (Local)' : 'Post Tag Currency (Local)',
	'Post Tag OTE (in Local Currency)' : 'Post Tag OTE (in Local Currency)',
	'Post Tag OTE Override (Local Currency)' : 'Post Tag OTE Override (Local Currency)',
	'Post Tag Fringes (in Local Currency)' : 'Post Tag Fringes (in Local Currency)',
	'Post Tag OTE (USD)' : 'Post Tag OTE (USD)',
	'Post Tag Fringes (USD)' : 'Post Tag Fringes (USD)',
	'Prospective Outlook (CY) USD' : 'Prospective Outlook (CY) USD',
	'Cost' : 'Cost',
	'Cost with Lever Intermediary' : 'Cost with Lever Intermediary',
	'Cost with Lever breakup' : 'Cost with Lever breakup',
	'Promo in Employee Subband' : 'Promo in Employee Subband',
	'Promo in Employee AID' : 'Promo in Employee AID',
	'Ana ID Code of Promo In Emp' : 'Ana ID Code of Promo In Emp',
	'Promo-In Employee ID' : 'Promo-In Employee ID',
	'Strategy 2.0 Skills' : 'Strategy 2.0 Skills',
	'Counts' : 'Counts',
	'Unique Code Quartile' : 'Unique Code Quartile',
	'Final Band' : 'Final Band',
	'Final Country' : 'Final Country',
	'Final Job Family' : 'Final Job Family',
	'Final Currency' : 'Final Currency',
	'Outlook-HC' : 'Outlook-HC',
	'FY Exit HC' : 'FY Exit HC',
	'CTC $ - FY Exit' : 'CTC $ - FY Exit',
	'AVG HC' : 'AVG HC',
	'Position Count in HC' : 'Position Count in HC',
	'Final End Date' : 'Final End Date',
	'Check For Simulation' : 'Check For Simulation',
	'Test Ana ID' : 'Test Ana ID',
	'Quartile Count' : 'Quartile Count',
	'Total' : 'Total',
	'AOP Headcount' : 'AOP Headcount',
	'Female' : 'Female',
	'Active Churn Count' : 'Active Churn Count',
	'Active Churn Cost' : 'Active Churn Cost',
	'FY EXIT YTD' : 'FY EXIT YTD',
	'Position END - Promotion' : 'Position END - Promotion',
	'Position Vacant' : 'Position Vacant',
	'Lever Used' : 'Lever Used',
	'Wrong Lever' : 'Wrong Lever',
	'Final Version' : 'Final Version',
	'HC Exist against AOP' : 'HC Exist against AOP',
	'Occupied By HC' : 'Occupied By HC',
	'Occupied By JR Offered' : 'Occupied By JR Offered',
	'Opening For Promotion' : 'Opening For Promotion',
	'Promo Out.' : 'Promo Out.',
	'Promo IN.' : 'Promo IN.',
	'NA For Promotion' : 'NA For Promotion',
	'Available for Promotion' : 'Available for Promotion',
	'Available for Promotion On' : 'Available for Promotion On',
	'Promotion Flag' : 'Promotion Flag',
	'Error C/F Tag' : 'Error C/F Tag',
	'Conditional Formatting Flag' : 'Conditional Formatting Flag',
	'Reason (If Error in Simulation Tag)' : 'Reason (If Error in Simulation Tag)',
	'Date Flag' : 'Date Flag',
	'Band Rank.' : 'Band Rank.',
	'BU Code' : 'BU Code',
	'LOB Code' : 'LOB Code',
	'VERTICAL Code' : 'VERTICAL Code',
	'Position Start Date' : 'Position Start Date',
	'Levers Counts' : 'Levers Counts',
	'AOP Budgeted Cost' : 'AOP Budgeted Cost',
	'Low Cost - Total cost' : 'Low Cost - Total cost',
	'High Cost - Total cost' : 'High Cost - Total cost',
	'Low Cost - Headcount' : 'Low Cost - Headcount',
	'High cost - Headcount' : 'High cost - Headcount',
	'Parent Country' : 'Parent Country',
	'(Final Country)' : '(Final Country)',
	'(Cost Center)' : '(Cost Center)',
	'(Sub Band)' : '(Sub Band)',
	'bu' : 'bu',
	'Unique DFM Code' : 'Unique DFM Code',
	'Test - DFM Ind. HC' : 'Test - DFM Ind. HC',
	'Test - DFM Int. HC' : 'Test - DFM Int. HC',
	'Test - DFM Low Cost' : 'Test - DFM Low Cost',
	'Test - DFM High Cost' : 'Test - DFM High Cost',
	'Balanced AOP SLA' : 'Balanced AOP SLA',
	'HC Count.' : 'HC Count.',
	'HC-HC' : 'HC-HC',
	'LC-EXIT' : 'LC-EXIT',
	'HC-EXIT' : 'HC-EXIT',
	'Offered Female' : 'Offered Female',
	'Promo-IN Count' : 'Promo-IN Count',
	'Parent Position ID' : 'Parent Position ID',
	'Parent of Position ID' : 'Parent of Position ID',
	'Emp. for country' : 'Emp. for country',
	'Anaplan Code' : 'Anaplan Code',
	'Outlook-cost' : 'Outlook-cost',
	'JR Tag' : 'JR Tag',
	'position count' : 'position count',
	'FY Exit OTE' : 'FY Exit OTE',
	'FY Exit Sub-Band' : 'FY Exit Sub-Band',
	'FY Exit Country' : 'FY Exit Country',
	'FY Exit Currency' : 'FY Exit Currency',
	'AOP-Increment % with increment FY Exc. Rate' : 'AOP-Increment % with increment FY Exc. Rate',
	'AOP-Increment Override % with increment FY Exc. Rate' : 'AOP-Increment Override % with increment FY Exc. Rate',
	'Outlook Cost Variation' : 'Outlook Cost Variation',
	'Filter View' : 'Filter View',
	'Overall Balance / Open / Offered Position' : 'Overall Balance / Open / Offered Position',
	'Outlook Annualized Cost' : 'Outlook Annualized Cost',
	'Dashboard 3 Cost' : 'Dashboard 3 Cost',
	'Previous Employee ID' : 'Previous Employee ID',
	'Current Period' : 'Current Period',
	'Check' : 'Check',
	'New Hire CY Cal' : 'New Hire CY Cal',
	'User-Matrix BU' : 'User-Matrix BU',
	'User-Matrix LOB' : 'User-Matrix LOB',
	'User-Matrix Vertical' : 'User-Matrix Vertical',
	'Region' : 'Region',
	'Job Family Cluster Count' : 'Job Family Cluster Count',
	'Job Family Cluster Cost' : 'Job Family Cluster Cost',
	'(Final Cost Center) Test' : '(Final Cost Center) Test',
	'OTE(USD)' : 'OTE(USD)',
	'DFM Fringes Low Cost' : 'DFM Fringes Low Cost',
	'DFM Fringes High Cost' : 'DFM Fringes High Cost',
	'DFM OTE Low Cost' : 'DFM OTE Low Cost',
	'DFM OTE High Cost' : 'DFM OTE High Cost',
	'MIS items' : 'MIS items',
	'Check Version' : 'Check Version',
	'City Cluster_HC' : 'City Cluster_HC',
	'Cost Tag' : 'Cost Tag',
	'DFM High Cost' : 'DFM High Cost',
	'DFM Ind. HC' : 'DFM Ind. HC',
	'DFM Int. HC' : 'DFM Int. HC',
	'DFM Low Cost' : 'DFM Low Cost',
	'Eligible for inc' : 'Eligible for inc',
	'Final Facility-AOP' : 'Final Facility-AOP',
	'Final Facility-Outlook' : 'Final Facility-Outlook',
	'Fringes.' : 'Fringes.',
	'Job family Cluster HC' : 'Job family Cluster HC',
	'LC-HC' : 'LC-HC',
	'New Hire CY' : 'New Hire CY',
	'Post Tag Facility' : 'Post Tag Facility',
	'Post Tag Entity' : 'Post Tag Entity',
	'Role Matrix' : 'Role Matrix',
	'VARPAY-Amount' : 'VARPAY-Amount',
	'Position ID for Overlapping' : 'Position ID for Overlapping',
	'Position Count for Actual Cost' : 'Position Count for Actual Cost',
	'Position Overlapping (Transition)' : 'Position Overlapping (Transition)',
	'BU for Geographical Selection' : 'BU for Geographical Selection',
	'Job Family Cluster for Geographical Selection' : 'Job Family Cluster for Geographical Selection',
	'Job Family Cluster for Geographical Selection_HC' : 'Job Family Cluster for Geographical Selection_HC',
	'Promo-In Position ID (AOP - FY)' : 'Promo-In Position ID (AOP - FY)',
	'Clear Promo (AOP - FY)' : 'Clear Promo (AOP - FY)',
	'Simulation Tag (AOP - FY)' : 'Simulation Tag (AOP - FY)',
	'Select for Simulation (AOP - FY)' : 'Select for Simulation (AOP - FY)',
	'Position Sub Band (AOP - FY)' : 'Position Sub Band (AOP - FY)',
	'Start Date (AOP - FY)' : 'Start Date (AOP - FY)',
	'Facility Country (AOP - FY)' : 'Facility Country (AOP - FY)',
	'Cost Center (AOP - FY)' : 'Cost Center (AOP - FY)',
	'OTE-(HeadCount/Offered) (AOP - FY)' : 'OTE-(HeadCount/Offered) (AOP - FY)',
	'Loaded Cost (USD) (AOP - FY)' : 'Loaded Cost (USD) (AOP - FY)',
	'Cost (AOP - FY)' : 'Cost (AOP - FY)',
	'Conditional Formatting Flag (AOP - FY)' : 'Conditional Formatting Flag (AOP - FY)',
	'Counts (AOP - FY)' : 'Counts (AOP - FY)',
	'Levers Counts (AOP - FY)' : 'Levers Counts (AOP - FY)',
	'AOP Budgeted Cost (AOP - FY)' : 'AOP Budgeted Cost (AOP - FY)',
	'Low Cost - Headcount (AOP - FY)' : 'Low Cost - Headcount (AOP - FY)',
	'High cost - Headcount (AOP - FY)' : 'High cost - Headcount (AOP - FY)',
	'(Final Country) (AOP - FY)' : '(Final Country) (AOP - FY)',
	'def country (AOP - FY)' : 'def country (AOP - FY)',
	'(Cost Center) (AOP - FY)' : '(Cost Center) (AOP - FY)',
	'(Sub Band) (AOP - FY)' : '(Sub Band) (AOP - FY)',
	'Budgeted Position (AOP - FY)' : 'Budgeted Position (AOP - FY)',
	'DFM Low Cost (AOP - FY)' : 'DFM Low Cost (AOP - FY)',
	'DFM High Cost (AOP - FY)' : 'DFM High Cost (AOP - FY)',
	'New Hire Tag (AOP - FY)' : 'New Hire Tag (AOP - FY)',
	'NH Counts (AOP - FY)' : 'NH Counts (AOP - FY)',
	'NH Cost (AOP - FY)' : 'NH Cost (AOP - FY)',
	'Growth Business NH Count (AOP - FY)' : 'Growth Business NH Count (AOP - FY)',
	'Growth Business NH Cost (AOP - FY)' : 'Growth Business NH Cost (AOP - FY)',
	'OTE Local without Increment  CY Exc. Rate (AOP - FY)' : 'OTE Local without Increment  CY Exc. Rate (AOP - FY)',
	'Benefits Local without Increment  CY Exc. Rate (AOP - FY)' : 'Benefits Local without Increment  CY Exc. Rate (AOP - FY)',
	'Load cost USD without Increment  CY Exc. Rate (AOP - FY)' : 'Load cost USD without Increment  CY Exc. Rate (AOP - FY)',
	'Cost column without Increment  CY Exc. Rate (AOP - FY)' : 'Cost column without Increment  CY Exc. Rate (AOP - FY)',
	'Local OTE Post Increment%  with  CY Exc.Rate (AOP - FY)' : 'Local OTE Post Increment%  with  CY Exc.Rate (AOP - FY)',
	'Benefits Local with Increment  CY Exc. Rate (AOP - FY)' : 'Benefits Local with Increment  CY Exc. Rate (AOP - FY)',
	'Load cost USD  with Increment  CY Exc. Rate (AOP - FY)' : 'Load cost USD  with Increment  CY Exc. Rate (AOP - FY)',
	'Cost column  with Increment  CY Exc. Rate (AOP - FY)' : 'Cost column  with Increment  CY Exc. Rate (AOP - FY)',
	'Local OTE Post Increment%  with  FY Exc.Rate (AOP - FY)' : 'Local OTE Post Increment%  with  FY Exc.Rate (AOP - FY)',
	'Benefits Local with increment FY Exc. Rate (AOP - FY)' : 'Benefits Local with increment FY Exc. Rate (AOP - FY)',
	'Load cost USD with increment FYExc. Rate (AOP - FY)' : 'Load cost USD with increment FYExc. Rate (AOP - FY)',
	'OTE After Lever (AOP - FY)' : 'OTE After Lever (AOP - FY)',
	'Benefits After Lever (AOP - FY)' : 'Benefits After Lever (AOP - FY)',
	'Loaded Cost (USD) After Lever (AOP - FY)' : 'Loaded Cost (USD) After Lever (AOP - FY)',
	'AOP FY EXIT HC (AOP - FY)' : 'AOP FY EXIT HC (AOP - FY)',
	'AOP FY EXIT COST (AOP - FY)' : 'AOP FY EXIT COST (AOP - FY)',
	'AOP FY Count (AOP - FY)' : 'AOP FY Count (AOP - FY)',
	'AOP FY OTE (AOP - FY)' : 'AOP FY OTE (AOP - FY)',
	'AOP FY Benefits (AOP - FY)' : 'AOP FY Benefits (AOP - FY)',
	'NH Band (AOP - FY)' : 'NH Band (AOP - FY)',
	'AOP FY Year (AOP - FY)' : 'AOP FY Year (AOP - FY)',
	'AOP FY Sub Band (AOP - FY)' : 'AOP FY Sub Band (AOP - FY)',
	'Total Varriable pay (AOP - FY)' : 'Total Varriable pay (AOP - FY)',
	'Basic Salary (AOP - FY)' : 'Basic Salary (AOP - FY)',
	'RETRIALS (AOP - FY)' : 'RETRIALS (AOP - FY)',
	'Period for Fixed pay (AOP - FY)' : 'Period for Fixed pay (AOP - FY)',
	'allowance (AOP - FY)' : 'allowance (AOP - FY)',
	'HRA (AOP - FY)' : 'HRA (AOP - FY)',
	'Statutory Bonus (AOP - FY)' : 'Statutory Bonus (AOP - FY)',
	'Fixed Salary (AOP - FY)' : 'Fixed Salary (AOP - FY)',
	'Promo Changed Sub Band. (AOP - FY)' : 'Promo Changed Sub Band. (AOP - FY)',
	'AOP FY Country (AOP - FY)' : 'AOP FY Country (AOP - FY)',
	'AOP FY Currency (AOP - FY)' : 'AOP FY Currency (AOP - FY)',
	'Band For Fixed Sal IND (AOP - FY)' : 'Band For Fixed Sal IND (AOP - FY)',
	'Band For Fixed Sal INT (AOP - FY)' : 'Band For Fixed Sal INT (AOP - FY)',
	'%deduction in Variable (AOP - FY)' : '%deduction in Variable (AOP - FY)',
	'Total varriable pay before lever (AOP - FY)' : 'Total varriable pay before lever (AOP - FY)',
	'Budgeted CTC after lever (AOP - FY)' : 'Budgeted CTC after lever (AOP - FY)',
	'Budgeted CTC before lever (AOP - FY)' : 'Budgeted CTC before lever (AOP - FY)',
	'New Hire CY Cal (AOP - FY)' : 'New Hire CY Cal (AOP - FY)',
	'MIS Items (AOP - FY)' : 'MIS Items (AOP - FY)',
	'Position AOP End Date (AOP - FY)' : 'Position AOP End Date (AOP - FY)',
	'Position AOP Start Date (AOP - FY)' : 'Position AOP Start Date (AOP - FY)',
	'Actual Start Date of Position (AOP - FY)' : 'Actual Start Date of Position (AOP - FY)',
	'Post Tag Entity (AOP - FY)' : 'Post Tag Entity (AOP - FY)',
	'Attrition Replacement' : 'Attrition Replacement',
	'Final Sub Band of Position (AOP - FY)' : 'Final Sub Band of Position (AOP - FY)',
	'Employee Sub Band (AOP - FY)' : 'Employee Sub Band (AOP - FY)',
	'International Region (AOP - FY)' : 'International Region (AOP - FY)',
	'Revised Quartile (AOP - FY)' : 'Revised Quartile (AOP - FY)',
	'Local Currency (AOP - FY)' : 'Local Currency (AOP - FY)',
	'OTE (Local Currency) (AOP - FY)' : 'OTE (Local Currency) (AOP - FY)',
	'Fringes (Local Currency) (AOP - FY)' : 'Fringes (Local Currency) (AOP - FY)',
	'Loaded Cost (Local Currency) (AOP - FY)' : 'Loaded Cost (Local Currency) (AOP - FY)',
	'Prospective Outlook (CY) USD (AOP - FY)' : 'Prospective Outlook (CY) USD (AOP - FY)',
	'Cost with Lever Intermediary (AOP - FY)' : 'Cost with Lever Intermediary (AOP - FY)',
	'Cost with Lever breakup (AOP - FY)' : 'Cost with Lever breakup (AOP - FY)',
	'Unique Code Quartile (AOP - FY)' : 'Unique Code Quartile (AOP - FY)',
	'Final End Date (AOP - FY)' : 'Final End Date (AOP - FY)',
	'Lever Used (AOP - FY)' : 'Lever Used (AOP - FY)',
	'Low Cost - Total cost (AOP - FY)' : 'Low Cost - Total cost (AOP - FY)',
	'High Cost - Total cost (AOP - FY)' : 'High Cost - Total cost (AOP - FY)',
	'Parent Country (AOP - FY)' : 'Parent Country (AOP - FY)',
	'DFM Ind. HC (AOP - FY)' : 'DFM Ind. HC (AOP - FY)',
	'DFM Int. HC (AOP - FY)' : 'DFM Int. HC (AOP - FY)',
	'Vertical (AOP - FY)' : 'Vertical (AOP - FY)',
	'Job Level (AOP - FY)' : 'Job Level (AOP - FY)',
	'OTE After Increment (AOP - FY)' : 'OTE After Increment (AOP - FY)',
	'OTE (USD) (AOP - FY)' : 'OTE (USD) (AOP - FY)',
	'Fringes (USD) (AOP - FY)' : 'Fringes (USD) (AOP - FY)',
	'Promo in Employee Subband (AOP - FY)' : 'Promo in Employee Subband (AOP - FY)',
	'Balanced AOP SLA (AOP - FY)' : 'Balanced AOP SLA (AOP - FY)',
	'LOB/Segment (AOP - FY)' : 'LOB/Segment (AOP - FY)',
	'BU/SSU (AOP - FY)' : 'BU/SSU (AOP - FY)'
}

# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

def connection_open():
	con = pdb.connect("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(config['connection']['server']) +
			";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) + ";pwd=" +
			 str(config['connection']['password']))
	#con = pdb.connect("DRIVER={ODBC Driver 13 for SQL Server};SERVER=tcp:datawarempqxi6rtp4ycqdatawarehouse.database.windows.net;DATABASE="
	#		"TATA_Anaplan;UID=dataware;pwd=Celebal@1234")
	# engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
	cur = con.cursor()
	return cur

def call_proc_db():
	cursor = connection_open()
	cur.execute("""exec proc_name_1; exec proc_name2""")
	cur.commit()

def main_func(source_file):

	print("source_file: ", source_file)
	#Logic to insert data into Simulation_CY staging table from Anaplan csv file
	raw_file = pd.read_csv(source_file, skiprows = [0])
	raw_file_col = raw_file.columns.tolist()

	#Make the column strings here
	col_mapping_vlaues = COLUMN_MAPPINGS.values()
	col_list_insert_str = '('
	col_str_values = '('
	col_list_true = []
	for col in raw_file_col:
		if col in COLUMN_MAPPINGS:
			col_list_true.append(COLUMN_MAPPINGS[col])
			col_list_insert_str += '[' + str(COLUMN_MAPPINGS[col]) + '],'
			col_str_values += '?, '
	col_list_insert_final = col_list_insert_str[:-1]
	col_list_insert_final += ')'
	col_str_values_final = col_str_values[:-2]
	col_str_values_final += ')'

	#establish a connection
	cur = connection_open()
	cur.fast_executemany = True

	raw_file_na = raw_file.fillna('')
	print("start")
	start = time.clock()

	#insert data into table using bulk query
	rows_list = []
	print(len(raw_file_na))
	count = 0
	# raw_file_na_1 = raw_file_na[:1]
	for index, row in raw_file_na.iterrows():
		count += 1
		data = ()
		for col in col_list_true:
			data = data + (str(row[col]), )
		# data = tuple(col for col in row if row[0] in col_mapping_vlaues)
		rows_list.append(data)
		#print('values_str: ', values_str_final)
		#print('col_list_final: ', col_list_final)
		if count % 5000 == 0:
			print(count)
			bulk_query = "insert into STG_Simulation_CY_BUHR " + col_list_insert_final + " values " + str(col_str_values_final)
			#print('bulk_query: ', rows_list)
			cur.executemany(bulk_query, rows_list)
			cur.commit()
			temp_str = '('
			print("bulk insert query fired")
	else:
		print(index, "Inside else")
		bulk_query = "insert into STG_Simulation_CY_BUHR " + col_list_insert_final + " values " + str(col_str_values_final)
		# print("rows_list: ", rows_list)
		# print("bulk_query: ", bulk_query)
		cur.executemany(bulk_query, rows_list)
		cur.commit()
		print("bulk insert query fired")
	print("end")
	print(time.clock() - start)

	#call db procedure in functio
	#call_proc_db()

if __name__ == '__main__':
	# source_file = 'D:/Mahak/Simulation/dev/Simulation - CY.csv'
	source_file = '/home/vineet/POCs/POC_6/Anaplan/anaplan-connect-1-3-3-3/Simulation - CY - Incremental Data-V1.csv'
	main_func(source_file)
