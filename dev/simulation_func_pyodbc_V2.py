import sys
import configparser
import time
import sys
import csv
import os
from datetime import datetime
import pandas as pd
import numpy as np
import pyodbc as pdb
# from subprocess import call
# call(['bash', '/home/vineet/POCs/POC_6/Anaplan/anaplan-connect-1-3-3-3/SimulationCYIncrementalData-V1.sh'])
# call(['bash', '/home/vineet/POCs/POC_6/Anaplan/anaplan-connect-1-3-3-3/UpdatesReplica-V1.sh'])

# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

def connection_open():
	con = pdb.connect("DRIVER={" + str(config['connection']['driver']) + "};SERVER=" + str(config['connection']['server']) +
			";DATABASE=" + str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) + ";pwd=" +
			 str(config['connection']['password']))
	#con = pdb.connect("DRIVER={ODBC Driver 13 for SQL Server};SERVER=tcp:datawarempqxi6rtp4ycqdatawarehouse.database.windows.net;DATABASE="
	#		"TATA_Anaplan;UID=dataware;pwd=Celebal@1234")
	# engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
	cur = con.cursor()
	return cur

def call_proc_db():
	cursor = connection_open()
	cur.execute("""exec proc_name_1; exec proc_name2""")
	cur.commit()

def main_func(source_file):

	print("source_file: ", source_file)
	#Logic to insert data into Simulation_CY staging table from Anaplan csv file
	raw_file = pd.read_csv(source_file, skiprows = [0])
	raw_file_col = raw_file.columns.tolist()

	col_list_insert_str = '('
	col_str_values = '('
	for col in raw_file_col:
		col_list_insert_str += '[' + str(col) + '],'
		col_str_values += '?, '
	col_list_insert_final = col_list_insert_str[:-1]
	col_list_insert_final += ')'
	col_list_final = col_list_str[:-2]
	col_list_final += ' )'
	col_str_values_final = col_str_values[:-2]
	col_str_values_final += ')'
	cur = connection_open()
	cur.fast_executemany = True

	#drop existing table from DB
	cur.execute("""drop table STG_Simulation_CY""")
	cur.commit()
	print("table dropped")

	#Create new table with schema same as file we downloaded from Anaplan.
	cur.execute(col_list_final)
	cur.commit()
	print("table created")

	raw_file_na = raw_file.fillna('')
	print("start")
	start = time.clock()

	#insert data into table using bulk query

	rows_list = []
	print(len(raw_file_na))
	count = 0;
	#raw_file_na_1 = raw_file_na[:14000]
	for index, row in raw_file_na.iterrows():
		count += 1
		data = tuple(col for col in row)
		rows_list.append(data)
		#print('values_str: ', values_str_final)
		#print('col_list_final: ', col_list_final)
		if count % 5000 == 0:
			print(count)
			bulk_query = "insert into STG_Simulation_CY " + col_list_insert_final + " values " + str(col_str_values_final)
			#print('bulk_query: ', rows_list)
			cur.executemany(bulk_query, rows_list)
			cur.commit()
			temp_str = '('
			print("bulk insert query fired")
	else:
		print(index, "Inside else")
		bulk_query = "insert into STG_Simulation_CY " + col_list_insert_final + " values " + str(col_str_values_final)
		print("rows_list: ", rows_list)
		print("bulk_query: ", bulk_query)
		cur.executemany(bulk_query, rows_list)
		cur.commit()
		print("bulk insert query fired")
	print("end")
	print(time.clock() - start)

	#call db procedure in functio
	#call_proc_db()

if __name__ == '__main__':
	# source_file = 'D:/Mahak/Simulation/dev/Simulation - CY.csv'
	source_file = '/home/vineet/POCs/POC_6/Anaplan/anaplan-connect-1-3-3-3/Simulation - CY - Incremental Data-V1.csv'
	main_func(source_file)
