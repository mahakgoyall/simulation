import sys
import configparser
import time
import sys
import csv
import os
import logging
from logging.config import fileConfig
from datetime import datetime
import pandas as pd
import numpy as np
import pyodbc as pdb

# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

def connection_open():
	con = pdb.connect("DRIVER={" + str(config['connection']['driver']) +
								"};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
								str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
								";pwd=" + str(config['connection']['password']))
	# engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
	cur = con.cursor()
	return cur

def main_func(source_file):

	print("source_file: ", source_file)
	#Logic to insert data into Simulation_CY staging table from Anaplan csv file
	# try:
	raw_file = pd.read_csv(source_file, skiprows = [0])
	raw_file_col = raw_file.columns.tolist()
	col_list_str = 'CREATE TABLE Simulation_CY_STG ( '
	col_list_insert_str = '('
	for col in raw_file_col:
		col_list_str += '[' + str(col) + '] nvarchar(500), '
		col_list_insert_str += '[' + str(col) + '],'
	col_list_insert_final = col_list_insert_str[:-1]
	col_list_insert_final += ')'
	col_list_final = col_list_str[:-2]
	col_list_final += ' )'
	cur = connection_open()
	cur.fast_executemany = True

	#drop existing table from DB
	cur.execute("""drop table Simulation_CY_STG""")
	cur.commit()
	print("table dropped")

	#Create new table with schema same as file we downloaded from Anaplan.
	cur.execute(col_list_final)
	cur.commit()
	print("table created")

	raw_file_na = raw_file.fillna('')
	print("start")
	start = time.clock()

	rows_list = []
	values_str = '('
	print(len(raw_file_na))
	count = 0
	raw_file_na_1 = raw_file_na[:6]
	for index, row in raw_file_na_1.iterrows():
		count += 1
		data = tuple(col for col in row)
		for col in row:
			values_str += '?, '
		rows_list.append(data)
		values_str_final = values_str[:-2]
		values_str_final += ')'
		# print('temp_str: ', temp_str_final)
		# print('values_str: ', values_str_final)
		# print('col_list_final: ', col_list_final)
		# print('bulk_query: ', bulk_query)
		if count % 500 == 0: 
			print(count)
			bulk_query = "insert into Simulation_CY_STG " + col_list_insert_final + " values " + str(values_str_final)
			print('bulk_query: ', rows_list)
			cur.executemany(bulk_query, rows_list)
			cur.commit()
			temp_str = '('
			print("bulk insert query fired")
	else:
		print(index, "Inside else")
		bulk_query = "insert into Simulation_CY_STG " + col_list_insert_final + "   " + str(values_str_final)
		print("bulk_query: ", rows_list)
		cur.executemany(bulk_query, rows_list)
		cur.commit()
		print("bulk insert query fired")
	print("end")
	print(time.clock() - start)

if __name__ == '__main__':
	source_file = 'D:/Mahak/Simulation/dev/Simulation - CY3txtmahak.csv'
	main_func(source_file)
