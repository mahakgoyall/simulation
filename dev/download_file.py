import sys
import urllib.parse
import sqlalchemy
import yagmail
import configparser
from selenium import webdriver
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions
from selenium.webdriver.common.by import By
from os import listdir
from selenium.common.exceptions import NoSuchElementException, NoSuchAttributeException, ElementNotVisibleException, \
    WebDriverException
from selenium.webdriver.common.keys import Keys
import time
from bs4 import BeautifulSoup
import sys
import csv
import os
import logging
from logging.config import fileConfig
import glob
import shutil
from datetime import datetime
from send_mail import send_email
from screenshots import screenshots
import pandas as pd
import numpy as np

# configuration file setup
config = configparser.ConfigParser()
config.read(os.path.join(os.path.abspath(os.path.dirname(__file__)), 'config.ini'))

# yagmail setup
# yagmail.register(str(config['mail']['sender_mail']), str(config['mail']['email_password']))
# yag = yagmail.SMTP(str(config['mail']['sender_mail']))
COLUMN_MAPPINGS = {
        'Username' : 'Username',
        'Employee Status' : 'Employee_Status',
        'Position ID' : 'Position_ID',
        'First Name' : 'First_Name',
        'Middle Name' : 'Middle_Name',
        'Last Name' : 'Last_Name',
        'B  Email Information Email Address' : 'Business_Email_Information_Email_Address',
        'CTC-Currency' : 'CTC_Currency',
        'BU/SSU Description' : 'BU_SSU_Description',
        'LOB/Segment Description' : 'LOB_Segment_Description',
        'Vertical Description' : 'Vertical_Description',
        'Sub Vertical Description' : 'Sub_Vertical_Description',
        'Entity Description' : 'Entity_Description',
        'Reporting Manager User Sys ID' : 'Reporting_Manager_User_Sys_ID',
        'Reporting Manager' : 'Reporting_Manager',
        'BU HR SPOC  Job Relationships User ID' : 'BU_HR_SPOC_Job_Relationships_User_ID',
        'BU HR SPOC  Job Relationships Name' : 'BU_HR_SPOC_Job_Relationships_Name',
        'REGIONAL HR  Job Relationships User ID' : 'REGIONAL_HR_Job_Relationships_User_ID',
        'REGIONAL HR  Job Relationships Name' : 'REGIONAL_HR_Job_Relationships_Name',
        'Cost Center Description' : 'Cost_Center_Description',
        'International Region' : 'International_Region',
        'Facility Country' : 'Facility_Country',
        'Facility  Description' : 'Facility_Description',
        'Band/Sub Band  Description' : 'Band_Sub_Band_Description',
        'Job Level' : 'Job_Level',
        'Designation' : 'Designation',
        'Employment Details Tata Group Joining Date' : 'Employment_Details_Tata_Group_Joining_Date',
        'Employment Details Hire Date' : 'Employment_Details_Hire_Date',
        'Confirmation Date' : 'Confirmation_Date',
        'Employment Details Last Working Date' : 'Employment_Details_Last_Working_Date',
        'Employment Details Termination Date' : 'Employment_Details_Termination_Date',
        'Event' : 'Event',
        'Event Reason' : 'Event_Reason',
        'Employee Type' : 'Employee_Type',
        'Employee Class' : 'Employee_Class',
        'Job Family Description' : 'Job_Family_Description',
        'Internal Specialization Name' : 'Internal_Specialization_Name',
        'Job Code' : 'Job_Code',
        'Variable Pay Plan Type' : 'Variable_Pay_Plan_Type',
        'Last Promotion Date' : 'Last_Promotion_Date',
        'Gender' : 'Gender',
        'Date Of Birth' : 'Date_Of_Birth',
        'BU/SSU GMC Member' : 'BU_SSU_GMC_Member',
        'Title' : 'Title',
        'Entity Code' : 'Entity_SAP_Entity_Code',
        'BU/SSU Code' : 'BU_SSU_Code',
        'LOB/Segment Code' : 'LOB_Segment_Code',
        'Vertical Code' : 'Vertical_Code',
        'Cost Center Code' : 'Cost_Center_Code',
        'Job Code Job Code' : 'Job_Code_Job_Code',
        'Job Code Description' : 'Job_Code_Description',
        'FIXPAY-Amount' : 'FIXPAY_Amount',
        'RETIRALS-Amount' : 'RETIRALS_Amount',
        'VARPAY-Amount' : 'VARPAY_Amount',
        'OTE-Amount' : 'OTE_Amount',
        'CTC-Amount' : 'CTC_Amount',
        'Job Code Band/Sub-Band' : 'Job_Code_Band_Sub_Band',
        'Account ID' : 'Account_ID',
        'AnnualFringe-Amount' : 'AnnualFringe_Amount',
        'Sub Vertical Code' : 'Sub_Vertical_Code',
        'OTE-Amount' : 'OTE_Amount'
}
date_col_list = ['Employment_Details_Retirement_Date', 'BI_end_Date', 'Last_Modified_Date', 
    'Employment_Details_Retirement_Date', 'Last_Promotion_Date', 'Confirmation_Date', 
    'Employment_Details_Termination_Date', 'Employment_Details_Last_Working_Date',
    'Employment_Details_Hire_Date', 'Employment_Details_Tata_Group_Joining_Date', 'Date_Of_Birth']

def connection_open():
    params = urllib.parse.quote("DRIVER={" + str(config['connection']['driver']) +
                                "};SERVER=" + str(config['connection']['server']) + ";DATABASE=" +
                                str(config['connection']['database']) + ";UID=" + str(config['connection']['uid']) +
                                ";pwd=" + str(config['connection']['password']))
    engine = sqlalchemy.create_engine("mssql+pyodbc:///?odbc_connect=%s" % params)
    return engine


def file_moving(source, destination, keyword):
    date_time = list()
    file_list = list()

    for f in glob.glob(str(source + '/*' + keyword + '*')):
        try:
            file_list.append(f)
            Temp = f.split('/')
            Temp = Temp[Temp.__len__() - 1].split('.')
            file = ''.join(Temp[:Temp.__len__() - 1])
            Temp = file.split('_')
            print('last temp: ', Temp)
            date_time.append(
                datetime(int(Temp[5]), int(Temp[6]), int(Temp[7]), int(Temp[8]), int(Temp[9]), int(Temp[10])))
        except Exception as e:
            print('Error in file moving process.' + str(e))
    print('date time var: ', date_time)
    latest = max(date_time)
    index_date = date_time.index(latest)
    file = file_list[index_date]

    if not glob.glob(str(destination + '/master/*' + keyword + '*')):
        shutil.move(file, str(destination + '/master'))

    else:
        shutil.move(file, str(destination + '/archive'))

def file_dumping(destination):
    for master_file in glob.glob(str(destination + '/master/*.csv')):
        shutil.move(master_file, str(destination + '/archive'))
    print("file_dumping called. file moved to archive folder.")

def data_dump_func(source_file):

    engine = connection_open()

    #Drop the dump table holding the backup of main table
    con = engine.connect()
    trans = con.begin()
    con.execute("""IF OBJECT_ID('Fact_Employee_Central_bkp', 'U') IS NOT NULL DROP TABLE Fact_Employee_Central_bkp;""")
    trans.commit()
    print("dump table dropped.")

    #Create new backup table from main current table

    trans = con.begin()
    con.execute("""select * into Fact_Employee_Central_bkp from Fact_Employee_Central""")
    trans.commit()
    print("data movement to bkp table done.")

    #Drop index from table

    trans = con.begin()
    con.execute("""DROP INDEX IF EXISTS [CI_Eno] ON Fact_Employee_Central""")
    trans.commit()
    print("index dropping done.")

    # truncate the table before pushing any data in it
    
    trans = con.begin()
    con.execute("Truncate table Fact_Employee_Central")
    trans.commit()
    print("Table Fact_Employee_Central data truncated.")

    error_log_file = open('error-log.txt','w') 
    #Logic to insert data into Employee_Central_Fact table
    try:
        raw_file = pd.read_csv(source_file)
        raw_file_col = raw_file.columns.tolist()
        # print("raw_file_col: ", raw_file_col)
        # print(jhsuxsu)
        col_list_str = '('
        for col, value in COLUMN_MAPPINGS.items():
            col_list_str += '[' + str(value) + '],'
        col_list_final = col_list_str[:-1]
        col_list_final += ')'

        # raw_file = raw_file1[:10]
        raw_file_na = raw_file.fillna('')
        temp_str = '('
        print(len(raw_file_na))
        count = 0
        for index, row in raw_file_na.iterrows():
            count += 1
            for col, value in COLUMN_MAPPINGS.items():
                if value in date_col_list and not row[col] == '':
                    temp_str += "convert(datetime, '" + str(row[col]).replace("'", "''") + "', 103),"
                else:
                    temp_str += "'" + str(row[col]).replace("'", "''") + "',"
            temp_str = temp_str[:-1]
            temp_str += '),('
            # print('temp_str: ', temp_str)
            # print('col_list_final: ', col_list_final)
            if count % 500 == 0: 
                print(count)
                bulk_query = "insert into Fact_Employee_Central " + col_list_final + " values " + str(temp_str[:-2])
                trans = con.begin()
                con.execute(bulk_query)
                trans.commit()
                temp_str = '('
                print("bulk insert query fired")
        else:
            # print(index, "Inside else")
            bulk_query = "insert into Fact_Employee_Central " + col_list_final + " values " + str(temp_str[:-2])
            trans = con.begin()
            con.execute(bulk_query)
            trans.commit()
            print("bulk insert query fired")
    except Exception as e:
        print(str(e))
        error_log_file.write(str(e))
        error_log_file.close()
        sys.exit()
    print("data dumping done.")

    #creting the indexes in the table

    trans = con.begin()
    con.execute("""CREATE CLUSTERED INDEX CI_Eno ON Fact_Employee_Central([Username])""")
    trans.commit()
    print("created clustered index on table Fact_Employee_Central.")

    #Call the procedure to make the hierarchy table.
    #NOT REQUIRED ANYMORE

    # trans = con.begin()
    # con.execute("""insert into update_flag values ('1')""")
    # trans.commit()
    # print("hierarchy formed using update command.")

    #Append the time and count from table to some text file
    trans = con.begin()
    table_count = con.execute("""select count(*) from Fact_Employee_Central""").fetchall()
    trans.commit()
    error_log_file = open('error-log.txt','a')
    error_log_file.write("\n Table_ count is: "+ str(table_count[0][0])) 
    error_log_file.write("\n Date time is: " + str(datetime.now()))
    error_log_file.close()
    print("time and table count stored in error-log file.")


def remove_reports(browser):
    select_report_count = 5
    select_report = None
    while not select_report and select_report_count > 0:
        try:
            select_report_count -= 1
            select_report = browser.find_element_by_xpath('//input[@title="Select All"]')
            select_report.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not select_report and select_report_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find Select all checkbox on Analytics page.')
        sys.exit(str(config['system']['exit_code']))

    remove_report_count = 5
    remove_report = None
    while not remove_report and remove_report_count > 0:
        try:
            remove_report_count -= 1
            remove_report = browser.find_element_by_xpath('//input[@value="Remove"]')
            remove_report.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not remove_report and remove_report_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find Remove button on Analytics page.')
        sys.exit(str(config['system']['exit_code']))


def main_func():
    engine = connection_open()
    print("entered in main function of report download for email updates.")

    try:
        browser = webdriver.Chrome(executable_path=str(config['system']['crome_driver_path']))
    except WebDriverException as e:
        print(str(e))
        sys.exit(str(config['system']['exit_code']))  # Type valid exit code

    browser.get(str(config['system']['web_url']))
    browser.maximize_window()

    username_var = None
    username_find_count = 5
    print("Trying to find username section")
    while username_find_count > 0 and not username_var:
        try:
            username_find_count -= 1
            username_var = browser.find_element_by_id('username')
            username_var.send_keys(str(config['system']['login_username']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not username_var and username_find_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find username field in login page')
        sys.exit(str(config['system']['exit_code']))

    password_var = None
    password_find_count = 5
    while password_find_count > 0 and not password_var:
        try:
            password_find_count -= 1
            password_var = browser.find_element_by_id('password')
            password_var.send_keys(str(config['system']['login_password']))
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not password_var and password_find_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find password field in login page')
        sys.exit(str(config['system']['exit_code']))

    login_link_count = 5
    login_link = None
    while login_link_count > 0 and not login_link:
        try:
            login_link_count -= 1
            login_link = browser.find_element_by_name('login')
            login_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if login_link_count <= 0 and not login_link:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find login button link in login page')
        sys.exit(str(config['system']['exit_code']))

    home_click_count = 5
    home_click = None
    while home_click_count > 0 and not home_click:
        try:
            home_click_count -= 1
            home_click = browser.find_element_by_id('customHeaderModulePickerBtn-content')
            home_click.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if home_click_count <= 0 and not home_click:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find home button attribute on home page')
        sys.exit(str(config['system']['exit_code']))
    while True:
        break

    time.sleep(3)
    calibration_link_count = 5
    calibration_link = None
    while not calibration_link and calibration_link_count > 0:
        try:
            calibration_link_count -= 1
            calibration_link = browser.find_element_by_link_text('Reports')
            calibration_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not calibration_link and calibration_link_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find Reports attribute in drop down list.')
        sys.exit(str(config['system']['exit_code']))

    reporting_var_count = 5
    reporting_var = None
    while not reporting_var and reporting_var_count > 0:
        try:
            reporting_var_count -= 1
            reporting_var = browser.find_element_by_link_text('Reporting')
            reporting_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not calibration_link and calibration_link_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find reporting link on Analytics page.')
        sys.exit(str(config['system']['exit_code']))

    # remove previously downloaded reports.
    scheduled_var_count = 5
    scheduled_var = None
    while not scheduled_var and scheduled_var_count > 0:
        try:
            scheduled_var_count -= 1
            scheduled_var = browser.find_element_by_link_text('Scheduled Reports')
            scheduled_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not scheduled_var and scheduled_var_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find Scheduled reports link on Analytics page.')
        sys.exit(str(config['system']['exit_code']))

    remove_flag_count = 5
    remove_flag = None
    while not remove_flag and remove_flag_count > 0:
        try:
            remove_flag_count -= 1
            remove_flag = browser.find_element_by_xpath('//td[text()="No report found."]')
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not remove_flag and remove_flag_count <= 0:
        pass
    if not remove_flag:
        remove_reports(browser)

    adhoc_var_count = 5
    adhoc_var = None
    while not adhoc_var and adhoc_var_count > 0:
        try:
            adhoc_var_count -= 1
            adhoc_var = browser.find_element_by_link_text('Ad Hoc Reports')
            adhoc_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not adhoc_var and adhoc_var_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find Ad Hoc reports link on Analytics page.')
        sys.exit(str(config['system']['exit_code']))

    CalibrationSample_count = 5
    CalibrationSample = None
    while not CalibrationSample and CalibrationSample_count > 0:
        try:
            CalibrationSample_count -= 1
            CalibrationSample = browser.find_element_by_link_text('For DataMart Fact_Employee_Central')
            CalibrationSample.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not CalibrationSample and CalibrationSample_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find For DataMart Fact_Employee_Central link on reports list.')
        sys.exit(str(config['system']['exit_code']))

    runoffile_link_count = 5
    runoffile_link = None
    while not runoffile_link and runoffile_link_count > 0:
        try:
            runoffile_link_count -= 1
            runoffile_link = browser.find_element_by_xpath('//label[text()="Run Offline"]')
            runoffile_link.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not runoffile_link and runoffile_link_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find run offline checkbox on pop-up.')
        sys.exit(str(config['system']['exit_code']))

    generate_btn_count = 5
    generate_btn = None
    while not generate_btn and generate_btn_count > 0:
        try:
            generate_btn_count -= 1
            generate_btn = browser.find_element_by_xpath('//button[text()="Generate Report"]')
            generate_btn.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not generate_btn and generate_btn_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find generate report button on pop-up.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(10)
    browser.refresh()

    # download the files for matching
    scheduled_var_count = 5
    scheduled_var = None
    while not scheduled_var and scheduled_var_count > 0:
        try:
            scheduled_var_count -= 1
            scheduled_var = browser.find_element_by_link_text('Scheduled Reports')
            scheduled_var.click()
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not scheduled_var and scheduled_var_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find scheduled reports link on analytics tab.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(10)
    browser.refresh()

    file_download = None
    file_download_count = 1
    while not file_download and file_download_count > 0:
        try:
            browser.refresh()
            time.sleep(15)
            file_downloads = browser.find_elements_by_link_text('1 file')
            if file_downloads:
                for file in file_downloads:
                    try:
                        file_list = []
                        file_list = file
                        file_download_count -= 1
                        file_list.click()
                    except Exception as e:
                        pass
            if file_download_count <= 0:
                file_download = True
        except NoSuchElementException as e:
            time.sleep(2)
        except NoSuchAttributeException as e:
            time.sleep(2)
        except ElementNotVisibleException as e:
            time.sleep(2)
        except Exception as e:
            time.sleep(2)
    if not file_download and file_download_count <= 0:
        browser.save_screenshot('error_download_foremail.png')
        send_email(screenshot_name, 'Couldn\'t find i file i.e. download file link on scheduled reports tab.')
        sys.exit(str(config['system']['exit_code']))

    time.sleep(60)
    print('file download is completed')
    browser.quit()
    print('File movement from foldrers start.')
    source = str(config['files']['source'])
    destination = str(config['files']['destination'])
    keyword = 'For_DataMart_Fact_Employee_Central'
    file_moving(source, destination, keyword)
    master_file = ''
    for file in glob.glob(destination + '/master/' + keyword +'*.csv'):
        master_file = file
    print("master_file path: ", master_file)
    #TODO: implement the dataframe to db movement logic
    data_dump_func(master_file)
    #Move file from master to archive folder.
    file_dumping(destination)


if __name__ == '__main__':
    # source_file = 'D:/EmployeeCentral/central/files/master/For_DataMart_Fact_Employee_Central_2018_04_13_04_31_02.csv'
    # data_dump_func(source_file)
    main_func()
